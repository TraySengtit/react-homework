import api from "../utils/api";

export const fetchArticles = async () => {
  let response = await api.get("articles");
  return response.data.data;
};
export const deleteArticle = async (id) => {
  let response = await api.delete("article/" + id);
  return response.data.message;
};

export const postArticle = async (article) => {
  let response = await api.post("/article", article);
  return response.data.data;
};

export const updateArticleById = async (id, updatedArticle) => {
  let response = await api.put("/article/" + id, updatedArticle);
  return response.data.message;
};

export const uploadImage = async (file) => {
  let formData = new FormData();
  formData.append("image", file);

  let response = await api.post("images", formData);
  return response.data.url;
};
