import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Menu from "./components/Menu";
import Post from "./pages/Post";
import Home from "./pages/Home";

function App() {
  return (
    <Router>
      <Menu />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/crud-article" component={Post} />
      </Switch>
    </Router>
  );
}

export default App;
