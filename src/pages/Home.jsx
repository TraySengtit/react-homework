import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Card } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  onDeleteArticle,
  onFetchAuthors,
} from "../redux/actions/articleAction";
import { NavLink } from "react-router-dom";
function Home() {
  const articles = useSelector((state) => state.articleReducer.articles);
  const dispatch = useDispatch();
  const [selectedId, setSelectedId] = useState("");

  useEffect(() => {
    dispatch(onFetchAuthors());
  }, []);

  const onDelete = (id) => {
    if (id === selectedId);
    dispatch(onDeleteArticle(id));
  };

  let articleCard = articles.map((article) => (
    <Col md={3} key={article._id}>
      <Card className="mb-2">
        <Card.Img
          variant="top"
          style={{ objectFit: "cover", height: "150px" }}
          src={
            article.image
              ? article.image
              : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
          }
        />
        <Card.Body>
          <Card.Title>{article.title}</Card.Title>
          <Card.Text className="text-line-1">{article.description}</Card.Text>
          <Button size="sm" variant="warning">
            Update
          </Button>{" "}
          <Button
            size="sm"
            variant="danger"
            onClick={() => onDelete(article._id)}
          >
            Delete
          </Button>{" "}
          <Button size="sm" variant="primary">
            More
          </Button>
        </Card.Body>
      </Card>
    </Col>
  ));

  return (
    <Container>
      <Row>
        <Col>
          <h3 style={{ display: "inline" }}>News Feed</h3>
          <Button
            className="mb-2 ms-4"
            variant="success"
            size="sm"
            as={NavLink}
            to="/crud-article"
          >
            Add Article
          </Button>
          <Row>{articleCard}</Row>
        </Col>
      </Row>
    </Container>
  );
}

export default Home;
