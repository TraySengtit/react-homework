import {
  fetchArticles,
  postArticle,
  updateArticleById,
  deleteArticle,
} from "../../services/articleService";
import { articleActionType } from "./articleActionType";

export const onFetchAuthors = () => async (dispatch) => {
  let articles = await fetchArticles();
  dispatch({
    type: articleActionType.FETCH_ARTICLES,
    payload: articles,
  });
};
export const onInsertArticle = (newArticle) => async (dispatch) => {
  let article = await postArticle(newArticle);
  dispatch({
    type: articleActionType.INSERT_ARTICLE,
    payload: article,
  });
};
export const onUpdateArticle =
  (articleId, updatedArticle) => async (dispatch) => {
    let message = await updateArticleById(articleId, updatedArticle);
    dispatch({
      type: articleActionType.UPDATE_ARTICLE,
      payload: { articleId, updatedArticle },
    });
  };
export const onDeleteArticle = (articleId) => async (dispatch) => {
  let message = await deleteArticle(articleId);
  dispatch({
    type: articleActionType.DELETE_ARTICLE,
    payload: articleId,
  });
};
