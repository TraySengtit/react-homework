export const articleActionType = {
  FETCH_ARTICLES: "FETCH_ARTICLES",
  DELETE_ARTICLE: "DELETE_ARTICLE",
  INSERT_ARTICLE: "INSERT_ARTICLE",
  UPDATE_ARTICLE: "UPDATE_ARTICLE",
};
