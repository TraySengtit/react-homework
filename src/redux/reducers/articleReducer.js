import { articleActionType } from "../actions/articleActionType";

const initialState = {
  articles: [],
};

const articleReducer = (state = initialState, action) => {
  switch (action.type) {
    case articleActionType.FETCH_ARTICLES:
      return { ...state, articles: [...action.payload] };
    case articleActionType.INSERT_ARTICLE:
      return { ...state, articles: [action.payload, ...state.articles] };
    case articleActionType.DELETE_ARTICLE:
      return {
        ...state,
        articles: state.articles.filter(
          (article) => article._id != action.payload
        ),
      };
    case articleActionType.UPDATE_ARTICLE:
      console.log(action);
      let newArticles = [...state.articles];
      newArticles = newArticles.map((article) => {
        if (article._id === action.payload.authorId) {
          article.name = action.payload.updatedArticle.name;
          article.email = action.payload.updatedArticle.email;
          article.image = action.payload.updatedArticle.image;
        }
        return article;
      });

      return { ...state, articles: newArticles };

    default:
      return state;
  }
};
export default articleReducer;
