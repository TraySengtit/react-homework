import React from "react";
import { useState } from "react";
import {
  Navbar,
  Container,
  Nav,
  Form,
  FormControl,
  Button,
  NavDropdown,
} from "react-bootstrap";

import { NavLink, Link } from "react-router-dom";

function NavMenu() {
  return (
    <Navbar bg="light" variant="light">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/">
          <img
            src="https://cdn3.iconfinder.com/data/icons/finance-system-media-ui-element-s25/512/news-512.png"
            style={{ width: "55px", marginTop: "-6px" }}
          ></img>
          News Today
        </Navbar.Brand>
        <Form className="d-flex">
          <FormControl
            type="search"
            placeholder="Search"
            className="mr-2"
            aria-label="Search"
          />
          <Button className="ms-2" variant="outline-success">
            Search
          </Button>
        </Form>
      </Container>
    </Navbar>
  );
}

export default NavMenu;
